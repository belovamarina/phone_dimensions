require 'rails_helper'

RSpec.describe MainController, type: :controller do
  describe 'GET index' do
    subject { get :index }

    it 'has a 200 status code' do
      expect(subject.status).to eq 200
    end
  end

  describe 'GET search' do
    let(:params) { { search: { query: 'iphone 10', crawler: 'PhonearenaCom' }, format: :js } }
    subject { get :search, params: params }

    it 'responds to js format' do
      expect(subject.content_type).to eq 'text/javascript'
    end

    context 'user send no query params' do
      let(:params) { {} }

      it { expect { subject }.to raise_error(ActionController::ParameterMissing) }
    end
  end
end
