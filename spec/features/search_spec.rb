require 'rails_helper'

RSpec.feature 'User is looking for phone dimensions', type: :feature do
  before do
    visit root_path
  end

  scenario 'user enters correct model phone', js: true do
    VCR.use_cassette('search/iphone_10') do
      fill_in 'search_query', with: 'iphone 10'
      click_button 'Search'

      expect(page).to have_content 'inches'
    end
  end

  scenario 'user enters incorrect model phone', js: true do
    VCR.use_cassette('search/ipphhone') do
      fill_in 'search_query', with: 'ipphhone'
      click_button 'Search'

      expect(page).to have_content 'not found'
    end
  end

  scenario 'network issues on the site', js: true do
    stub_request(:get, /phonearena.com/).to_return(status: [404, 'Not Found'])

    fill_in 'search_query', with: 'iphone 3'
    click_button 'Search'

    expect(page).to have_content '404 Not Found'
  end
end
