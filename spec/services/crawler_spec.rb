require 'rails_helper'

RSpec.describe Crawler do
  let(:crawler) { described_class.new('search') }
  let!(:domain) { stub_const('Crawler::DOMAIN', 'http://example.com/') }

  describe '#absolute_url' do
    it 'joins domain and relative path' do
      expect(crawler.absolute_url('products/iphone10')).to eq 'http://example.com/products/iphone10'
    end
  end

  describe '#visit' do
    it 'visit url and parse it' do
      stub_request(:get, /example.com/).to_return(status: 200, body: '<html>Hello</html>')
      crawler.visit('products/iphone10')
      expect(crawler.body.xpath('/html').text).to eq 'Hello'
    end
  end
end
