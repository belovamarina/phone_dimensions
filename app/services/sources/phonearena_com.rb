# Crawler search phone dimensions on phonearena.com site
class PhonearenaCom < Crawler
  DOMAIN = 'https://www.phonearena.com/'.freeze

  def parse
    visit "search?term=#{query}"
    phone_link = body.at_xpath("//div[@id='phones']/div[@class='s_listing']//h3/a/@href")&.text

    visit phone_link if phone_link.present?
    body.xpath("//li[span[contains(@title, 'Dimension')]]").text.strip
  end
end
