require 'open-uri'

# Parent class for all crawlers (app/services/sources directory) with common methods.
# Crawler class should contain DOMAIN constant and instance method :parse.
# Method :parse should return string with result, or empty string, or nil
class Crawler
  attr_accessor :body
  attr_reader :query

  def initialize(query)
    @query = Addressable::URI.escape(query)
  end

  # Modify relative URL to absolute.
  def absolute_url(href)
    Addressable::URI.join(self.class::DOMAIN, href).to_s
  end

  # Open URL via open-uri and parse response with Nokogiri
  def visit(url)
    url = absolute_url(url)
    Rails.logger.info "#{self.class} visiting: #{url}"
    @body = Nokogiri::HTML(open(url))
  end
end
