# This class is designed for launch crawlers (app/services/sources directory).
#  Accepts the arguments: the name of the crawler's class [String], and search query [String]
class CrawlerEngine
  def initialize(klass, query)
    @klass = klass.constantize.new(query)
  end

  # Returns result of crawler's work (string or nil) or error message
  def start
    @klass.parse
  rescue StandardError => e
    Rails.logger.error("Engine: `#{e.class}: #{e.message}`\n#{e.backtrace.join("\n")}")
    e.message
  end
end
