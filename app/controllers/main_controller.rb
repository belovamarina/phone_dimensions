class MainController < ApplicationController
  protect_from_forgery except: :search
  before_action :set_crawlers, only: :index

  def index; end

  def search
    message = Rails.cache.fetch(search_params.values_at(:crawler, :query).join(':'), expires_in: 3.hours) do
      crawler = CrawlerEngine.new(*search_params.values_at(:crawler, :query))
      crawler.start
    end

    @message = message.blank? ? 'not found' : message
  end

  private

  def search_params
    params.require(:search).permit(:query, :crawler)
  end

  def set_crawlers
    @crawlers = Dir.glob("#{Rails.root}/app/services/sources/*").map { |f| File.basename(f, '.rb') }
  end
end
